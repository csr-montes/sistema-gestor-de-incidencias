<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Pantalla principal
Route::get('/home', 'HomeController@index')->name('home');

// Incidencia
Route::get('/report', function () {
    return view('incidence/report');
})->name('report');
Route::post('/report', 'IncidentController@reportIncident')->name('reportIncident');

Route::get('/record', 'IncidentController@index')->name('record');
Route::get('/record-data', 'IncidentController@recordData')->name('incident.recordData');
Route::put('/record', 'IncidentController@updateIncident')->name('incident.updateIncident');
Route::get('/record/{incident_id}', 'IncidentController@deleteIncident')->name('incident.deleteIncident');
Route::get('/record/{incident_id}/tools', 'IncidentController@toolsIncident')->name('incident.toolsIncident')->middleware('role:admin');
Route::put('/record/{incident_id}/tools', 'IncidentController@closingIncident')->name('incident.closingIncident')->middleware('role:admin');

// Message
Route::get('/messages/api/{incident_id}', 'MessageController@fetchMessages')->middleware('auth');
use App\Http\Middleware\CheckMessageAuthor;
Route::get('/messages/{incident_id}', 'MessageController@index')->name('timeline')->middleware(CheckMessageAuthor::class);
Route::post('/messages', 'MessageController@sendMessage')->middleware('auth');

// User
Route::get('/users', 'UserController@index')->name('users.index')->middleware('role:admin');
Route::post('/users', 'UserController@store')->name('users.store')->middleware('role:admin');
Route::get('/users/new', 'UserController@create')->name('users.create')->middleware('role:admin');
Route::get('/users/{user}', 'UserController@show')->where('user', '[0-9]+')->name('users.show')->middleware('role:admin');
Route::get('/users/{user}/edit', 'UserController@edit')->name('users.edit')->middleware('role:admin');
Route::put('/users/{user}', 'UserController@update')->middleware('role:admin');
Route::delete('/users/{user}', 'UserController@destroy')->name('users.destroy')->middleware('role:admin');
Route::get('/users/changePassword', 'UserController@changePasswordView')->name('users.changePasswordView');
Route::post('/users/changePassword', 'UserController@changePassword')->name('users.changePassword');
