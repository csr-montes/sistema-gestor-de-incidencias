<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Closure extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function incident()
    {
        return $this->belongsTo(Incident::class);
    }
}
