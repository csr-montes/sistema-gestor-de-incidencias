<?php

namespace App\Http\Middleware;

use Closure;

class CheckMessageAuthor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( !$request->user()->hasIncident($request->incident_id) && !$request->user()->hasRole('admin') ) {
            return redirect()->route('record');
        }

        return $next($request);
    }
}
