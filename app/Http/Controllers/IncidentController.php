<?php

namespace App\Http\Controllers;

use App\Closure;
use Illuminate\Http\Request;

//
use Illuminate\Support\Facades\Auth;
use App\Incident;
use App\Message;

use DataTables;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class IncidentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        return view('incidence/record');
    }

    public function recordData()
    {

        if (Auth::user()->hasRole('admin')) {
            $incidents = Incident::select('id', 'category', 'urgency', 'state', 'subject', 'created_at', 'updated_at')
                ->with('users:users.id,users.name,users.email')
                ->get();
        }
        else {
            $user_id = Auth::user()->id;
            $incidents = Incident::select('id', 'category', 'urgency', 'state', 'subject', 'created_at', 'updated_at')
                ->with('users:users.id,users.name,users.email')
                ->whereHas('users', function ($q) use ($user_id) {
                    $q->where('user_id', $user_id);
                })
                ->get();
        }
        // dd($incidents);
        return DataTables::of($incidents)->make(true);
    }

    public function reportIncident()
    {
        $data = request()->validate([
            'category_opt' => 'required',
            'urgency_opt' => 'required',
            'subject_txt' => 'required',
            'detail_txt' => 'required',
        ], [
            'category_opt.required' => 'The category field is required',
            'urgency_opt.required' => 'The urgency field is required',
            'subject_txt.required' => 'The subject field is required',
            'detail_txt.required' => 'The detail field is required',
        ]);

        $user = Auth::user();

        $incident = new Incident();

        $incident->category = $data['category_opt'];
        $incident->urgency = $data['urgency_opt'];
        $incident->state = 'Nueva';
        $incident->subject = $data['subject_txt'];
        $incident->save();
        $user->incidents()->attach($incident);

        $message = new Message;
        $message->message = $data['detail_txt'];
        $message->user_id = $user->id;
        $message->incident_id = $incident->id;
        $message->save();

        // Obtengo los usuarios con rol admin (1)
        $usersAdmin = DB::table('users')
            ->select('users.name','users.email')
            ->join('role_user', 'role_user.user_id', '=', 'users.id')
            ->where('role_id','=', '1')
            ->get();

       $usersAdmin->push((object) array('name' => Auth::user()->name, 'email' => Auth::user()->email));

        // Mensaje
        $to_incident = $incident->id;
        $data = array(
            'category' => $data['category_opt'],
            'urgency' => $data['urgency_opt'],
            'subject' => $data['subject_txt']
        );

        foreach($usersAdmin as $value) {
            Mail::send('emails.notification_mail', $data, function($message) use ($value, $to_incident) {
                $message->to($value->email, $value->name)->subject('Incidencia #'.$to_incident.' | NUEVO');
                $message->from(env('MAIL_USERNAME'),'SGI');
            });
        }

        return redirect()->route('record');
    }

    public function updateIncident()
    {
        $data = request()->validate([
            'parameter_id' => 'required',
            'modal_subject' => 'required',
            'modal_id_message' => 'required',
            'modal_first_message' => 'required',
        ], [
            'parameter_id.required' => 'Required',
            'modal_subject.required' => 'The subject field is required',
            'modal_id_message.required' => 'Required',
            'modal_first_message.required' => 'The detail field is required',
        ]);

        $incident = Incident::find($data['parameter_id']);
        $incident->subject = $data['modal_subject'];
        $incident->save();

        $message = Message::find($data['modal_id_message']);
        $message->message = $data['modal_first_message'];
        $message->save();

        return redirect()->route('record');
    }

    public function deleteIncident($incidentId)
    {
        if (Auth::user()->hasRole('admin')) {
            $affectedRows = Incident::where('id', '=', $incidentId)->delete();
        }
        else {
            $incident = Incident::find($incidentId);
            $incident->state = 'Anulado';
            $incident->save();
        }
        return redirect()->route('record');
    }

    public function toolsIncident($incidentId)
    {
        $incidents = Incident::select('id', 'category', 'urgency', 'state', 'subject', 'created_at', 'updated_at')
            ->where('id', $incidentId)
            ->with('users:users.id,users.name,users.email')
            ->get()
            ->first();

        if (Closure::select('datail_closing')->where('incident_id', $incidentId)->count() > 0) {
            $closing = DB::table('closures')
                ->select('closures.datail_closing','users.name')
                ->join('users', 'closures.user_id', '=', 'users.id')
                ->where('incident_id','=', $incidentId)
                ->get()
                ->first();

            // Consulta imperfecta
            // $closing = Closure::with(
            //         array('user'=>function($query){
            //             $query->select('id','name');
            //         }))
            //     ->where('incident_id', $incidentId)
            //     ->get();

            return view('incidence/tools', ['user' => Auth::user(), 'incidents' => $incidents, 'closing' => $closing]);
        }
        return view('incidence/tools', ['user' => Auth::user(), 'incidents' => $incidents]);
        // return dd($closing->datail_closing);
    }

    public function closingIncident($incidentId, Request $request)
    {
        if ($request->update == 'update') {
            $data = request()->validate([
                'category_opt' => 'required',
                'urgency_opt' => 'required',
                'subject' => 'required',
                'state_opt' => 'required',
                'datail_closing' => 'nullable',
            ]);

            $incident = Incident::find($incidentId);
            $incident->category = $data['category_opt'];
            $incident->urgency = $data['urgency_opt'];
            $incident->subject = $data['subject'];

            $incident->state = $data['state_opt'];
            $incident->save();

            return redirect()->back();

        } elseif($request->closing == 'closing') {
            $data = request()->validate([
                'category_opt' => 'required',
                'urgency_opt' => 'required',
                'subject' => 'required',
                'state_opt' => 'required',
                'datail_closing' => 'nullable',
            ]);

            $incident = Incident::find($incidentId);
            $incident->category = $data['category_opt'];
            $incident->urgency = $data['urgency_opt'];
            $incident->subject = $data['subject'];

            $incident->state = 'Cerrada';
            $incident->save();

            $closure = new Closure();
            $closure->datail_closing = $data['datail_closing'];
            $closure->user_id = Auth::user()->id;
            $closure->incident_id = $incident->id;
            $closure->save();

            // Obtengo los usuarios con rol admin (1)
            $usersAdmin = DB::table('users')
                ->select('users.name','users.email')
                ->join('role_user', 'role_user.user_id', '=', 'users.id')
                ->where('role_id','=', '1')
                ->get();

            // Mensaje
            $to_incident = $incident->id;
            $data = array(
                'category' => $data['category_opt'],
                'urgency' => $data['urgency_opt'],
                'subject' => $data['subject'],
                'detailClosing' => $data['datail_closing'],
                'closedBy' => Auth::user()->name
            );

            foreach($usersAdmin as $value) {
                Mail::send('emails.notification_mail', $data, function($message) use ($value, $to_incident) {
                    $message->to($value->email, $value->name)->subject('Incidencia #'.$to_incident.' | CERRADO');
                    $message->from(env('MAIL_USERNAME'),'SGI');
                });
            }

            return redirect()->back();

        } elseif($request->revert_closing == 'revert_closing') {
            $incident = Incident::find($incidentId);
            $incident->state = 'En Progreso';
            $incident->save();

            $deletedRows = Closure::where('incident_id', $incidentId)->delete();

            return redirect()->back();
        }

        // return redirect()->route('record');
    }

    public static function getIncidentStatus($incidentId)
    {
        $status = Incident::where('id', $incidentId)->select('state')->get()->first();

        return $status;
    }
}
