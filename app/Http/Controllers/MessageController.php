<?php

namespace App\Http\Controllers;

//
use App\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Events\MessageSentEvent;

use App\Incident;

class MessageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Obtiene mensajes de una conversacion.
     *
     * @param  $incidentId
     * @return void
     */
    public function fetchMessages($incidentId)
    {
        return Message::with(['user:id,name'])->where('incident_id', $incidentId)->get();
    }

    /**
     * Persist message to database
     *
     * @param  Request $request
     * @return Response
     */
    public function sendMessage(Request $request)
    {
        $user = Auth::user();

        $message = new Message;
        $message->message = $request->message;
        $message->user()->associate(Auth::user());
        $message->incident()->associate($request->incidentid);
        $message->save();

        broadcast(new MessageSentEvent($user, $message))->toOthers();
        return ['status' => 'Mensaje enviado!'];
    }

    public function index($incident_id)
    {
        $last_message = Message::select('user_id')->where('incident_id', $incident_id)->latest()->first();

        $incident = Incident::find($incident_id);
        if ($incident->state == 'Mensaje Nuevo' && Auth::user()->id != $last_message->user_id) {
            $incident->state = 'En Progreso';
            $incident->save();
        }

        // return dd($query);
        return view('chat.chatRoom')->with(['incidentID' => $incident_id]);
    }
}
