<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //$users = DB::table('users')->get();
        $users = User::all();

        $title = 'List of users';

        //return view('users.index')
        //    ->with('users', User::all())
        //    ->with('title', 'List of users');

        return view('users.index', compact('title', 'users'));
    }

    public function show(User $user)
    {
        return view('users.show', compact('user'));
    }

    public function create()
    {
        return view('users.create');
    }

    public function store()
    {
        $data = request()->validate([
            'name' => 'required',
            'email' => ['required', 'email', 'unique:users,email'],
            'password' => 'required|between:6,120',
        ], [
            'name.required' => 'The name field is required',
            'email.required' => 'The email field is required',
            'password.required' => 'The password field is required',
        ]);


        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password'])
        ]);

        return redirect()->route('users.index');
    }

    public function edit(User $user)
    {
        return view('users.edit', ['user' => $user]);
    }

    public function update(User $user)
    {
        $data = request()->validate([
            'name' => 'required',
            'email' => ['required', 'email', 'unique:users,email'],
            'password' => '',
        ]);

        if ($data['password'] != null) {
            $data['password'] = bcrypt($data['password']);
        } else {
            unset($data['password']);
        }

        $user->update($data);

        //return redirect("users/{$user->id}");
        return redirect()->route('users.show', ['user' => $user]);
    }

    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->route('users.index');
    }

    public function changePasswordView()
    {
        $user = Auth::user();
        return view('users.changePassword', ['user' => $user]);
    }

    public function changePassword(Request $request)
    {
        $this->validate($request, [
            'current_password' => 'required|min:6|max:32',
            'new_password' => 'required|min:6|max:32',
            'repeat_new_password' => 'required|min:6|max:32',
        ]);

        $user = Auth::user();

        if( Hash::check($request->current_password, $user->password)){
            if($request->new_password == $request->repeat_new_password){
                $password = bcrypt($request->new_password);
                $user->password = $password;
                $user->save();
                return redirect()->route('home')->withSuccess('Password actualizado');
            }
        }
        return redirect()->route('users.changePasswordView');

    }

}
