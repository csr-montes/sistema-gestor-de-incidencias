
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
Vue.use(require('vue-chat-scroll'))
window.moment = require('moment');
/* Vue.use(require('vue-moment')); */

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('chat-messages', require('./components/ChatMessages.vue').default);
Vue.component('chat-form', require('./components/ChatForm.vue').default);

const app = new Vue({
    el: '#aspp',
    data: {
        messages: [],
    },
    created() {

        Echo.private('chat')
        .listen('MessageSentEvent', (e) => {
            this.messages.push({
            message: e.message.message,
            user: e.user
            });
        });
    },
    /* mounted(){
        // this.fetchMessages(incident_id);
        Echo.private('chat')
            .listen('MessageSentEvent', (e) => {
            this.messages.push({
                message: e.message.message,
                user: e.user
            })
        })
    }, */
    methods: {
        /* fetchMessages(incident_id) {
            axios.get('/messages/'+incident_id).then(response => {
                this.messages = response.data;
            });
        }, */

        addMessage(message, incidentid) {
            this.messages.push(message);

            axios.post('/messages', message, incidentid).then(response => {
              console.log(response.data);
            });
        }
    }
});
