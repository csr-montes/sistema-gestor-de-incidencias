@extends('layouts.sidebar')


@section('content')
<!-- Success message for contact view
==================================== -->
@if(session('success'))
    <div class="alert alert-success page-alert" id="success-alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <strong>Cool!</strong> {{ session('success') }}
    </div>
@endif
<!-- ==================================== -->

<div class="container p-3">
    {{-- <div class="row justify-content-center">
        <div class="col-md-8"> --}}
            <div class="card">
                <div class="card-header">Bienvenido</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Usted esta conectado como {{ Auth::user()->name }} y tiene privilegios de
                    @if (Auth::user()->hasRole('Admin'))
                        Administrador
                    @else
                        Usuario
                    @endif
                </div>
            </div>
        {{-- </div>
    </div> --}}
</div>
@endsection

@section('scripts')
{{-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
@endsection
