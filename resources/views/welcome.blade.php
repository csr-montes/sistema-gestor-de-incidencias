<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="{{ asset('css/welcome.css') }}" rel="stylesheet">

        <!-- Scripts -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
        <script src="{{ asset('js/welcome.js') }}" defer></script>
    </head>

    <body>
        <div class="flex-center position-ref full-height">
            <div id="page-top">

                <!-- Navigation
                ==================================== -->
                <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
                    <div class="container">
                        <a class="navbar-brand js-scroll-trigger" href="#page-top">
                            {{ config('app.name') }}
                        </a>
        
                        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
        
                            <div class="collapse navbar-collapse" id="navbarResponsive">
                                <ul class="navbar-nav ml-auto">
        
                                    <li class="nav-item">
                                        <a class="nav-link js-scroll-trigger" href="#about">Sobre nosotros</a>
                                    </li>
        
                                    <li class="nav-item">
                                        <a class="nav-link js-scroll-trigger" href="#services">Caracter&iacute;sticas</a>
                                    </li>
        
                                    {{-- <li class="nav-item">
                                        <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
                                    </li> --}}
                                </ul>
                            </div>
                    </div>
                </nav>
                <!-- ==================================== -->
        
                <!-- Home
                ==================================== -->
                <header class="masthead text-center text-white d-flex">
                    <div class="container my-auto">
                        <div class="row">
                            <div class="col-lg-10 mx-auto">
                                <h1 class="text-uppercase">
                                    <strong>Sistema Gestor de Incidencias</strong>
                                </h1>
                                <hr>
                            </div>
        
                            <div class="col-lg-8 mx-auto">
                                <p class="text-faded mb-5">
                                    El Sistema Gestor de Incidencias es la soluci&oacute;n para administrar eficientemente los problemas ocurriedos dentro de tu organizaci&oacute;n!
                                </p>
                                @auth
                                    <a class="btn btn-primary btn-xl js-scroll-trigger" href="{{ url('/record') }}">Ingresar al sistema</a>
                                @else
                                    <a class="btn btn-primary btn-xl js-scroll-trigger" href="{{ route('login') }}">Iniciar sesi&oacute;n</a>
                                @endauth
                            </div>
                        </div>
                    </div>
                </header>
                <!-- ==================================== -->
        
                <!-- About Us
                ==================================== -->
                <section class="bg-dark" id="about">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 mx-auto text-center">
                                <h2 class="section-heading text-white">Bienvenidos!</h2>
                                <hr class="light my-4">
                                <p class="text-faded mb-4">El Sistema Gestor de Incidencias o SGI es un proyecto que nace de la necesidad observada en las organizaciones con poca digitalizaci&oacute;n en su n&uacute;cleo. Nosotros buscammos amenar el proceso a la hora de reportar las incidencias y darles seguimiento.</p>
                                <a class="btn btn-light btn-xl js-scroll-trigger" href="#services">Acerca del sistema</a>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- ==================================== -->
        
                <!-- Services
                ==================================== -->
                <section id="services">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12 text-center">
                                <h2 class="section-heading">Caracter&iacute;sticas</h2>
                                <hr class="my-4">
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 text-center">
                                <div class="service-box mt-5 mx-auto">
                                    <i class="fa fa-4x fa-users text-primary mb-3 sr-icons"></i>
                                    <h3 class="mb-3">Usuarios</h3>
                                    <p class="text-muted mb-0">Distincion de privilegios entre usuarios.</p>
                                </div>
                            </div>
                            {{-- <div class="col-lg-3 col-md-6 text-center">
                                <div class="service-box mt-5 mx-auto">
                                    <i class="fa fa-4x fa-paper-plane text-primary mb-3 sr-icons"></i>
                                    <h3 class="mb-3">Linea de Tiempo</h3>
                                    <p class="text-muted mb-0">Visualiza la historia de las incidencias en una linea de tiempo atractiva.</p>
                                </div>
                            </div> --}}
                            <div class="col-lg-6 col-md-6 text-center">
                                <div class="service-box mt-5 mx-auto">
                                    <i class="fa fa-4x fa-newspaper-o text-primary mb-3 sr-icons"></i>
                                    <h3 class="mb-3">Reportes</h3>
                                    <p class="text-muted mb-0">Reporta tu incidencia y dale seguimiento.</p>
                                </div>
                            </div>
                            {{-- <div class="col-lg-3 col-md-6 text-center">
                                <div class="service-box mt-5 mx-auto">
                                    <i class="fa fa-4x fa-heart text-primary mb-3 sr-icons"></i>
                                    <h3 class="mb-3">Interfaz</h3>
                                    <p class="text-muted mb-0">Reporte sus incidencias dentro de una interfaz amigable con el usuario.</p>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </section>
                <!-- ==================================== -->
        
                <!-- Contact
                ==================================== -->
                {{-- <section class="bg-dark text-white" id="contact">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-8 mx-auto text-center">
                                <h2 class="section-heading">Let's Get In Touch!</h2>
                                <hr class="light my-4">
                                <p class="mb-5">Ready to start your next project with us? That's great! Give us a call or send us an email and we will get back to you as soon as possible!</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 ml-auto text-center">
                                <i class="fa fa-phone fa-3x mb-3 sr-contact"></i>
                                <p>123-456-6789</p>
                            </div>
                            <div class="col-lg-4 mr-auto text-center">
                                <i class="fa fa-envelope-o fa-3x mb-3 sr-contact"></i>
                                <p>
                                    <a href="mailto:admin@domain.com">admin@domain.com</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </section> --}}
                <!-- ==================================== -->
            </div>
        </div>
    </body>
</html>