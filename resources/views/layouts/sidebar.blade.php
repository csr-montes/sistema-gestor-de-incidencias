<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('adminlte/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('adminlte/bower_components/Ionicons/css/ionicons.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('adminlte/dist/css/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
        folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('adminlte/dist/css/skins/_all-skins.min.css') }}">

    @yield('styles')

</head>

@if (Auth::user()->hasRole('Admin'))
    <body class="hold-transition skin-purple sidebar-mini">
@else
    <body class="hold-transition skin-yellow sidebar-mini">
@endif
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="{{ route('home') }}" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>SGI</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>SGI</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Sidebar toggle button-->

            <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        @if (Auth::user()->hasRole('Admin'))
                            <img src="{{ asset('adminlte/dist/img/avatar04.png') }}" class="user-image" alt="User Image">
                        @else
                            <img src="{{ asset('adminlte/dist/img/avatar.png') }}" class="user-image" alt="User Image">
                        @endif
                        <span class="hidden-xs">{{ Auth::user()->name }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            @if (Auth::user()->hasRole('Admin'))
                                <img src="{{ asset('adminlte/dist/img/avatar04.png') }}" class="img-circle" alt="User Image">
                            @else
                                <img src="{{ asset('adminlte/dist/img/avatar.png') }}" class="img-circle" alt="User Image">
                            @endif


                            <p>
                                {{ Auth::user()->name }}
                                <small>Registrado desde {{ Auth::user()->created_at->format('M Y') }}</small>
                            </p>
                        </li>
                        <!-- Menu Body -->
                        {{-- <li class="user-body">
                            <div class="row">
                                <div class="col-xs-4 text-center">
                                <a href="#">Followers</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                <a href="#">Sales</a>
                                </div>
                                <div class="col-xs-4 text-center">
                                <a href="#">Friends</a>
                                </div>
                            </div>
                        </li> --}}
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ route('users.changePasswordView') }}" class="btn btn-default btn-flat">Opciones</a>
                            </div>
                            <div class="pull-right">
                                <a class="btn btn-default btn-flat" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">{{ __('Cerrar Sesión') }}</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
            </div>
        </nav>
    </header>


    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
        <!-- Sidebar user panel -->
        @if (Auth::user()->hasRole('Admin'))

        @endif
        <div class="user-panel">
            @if (Auth::user()->hasRole('Admin'))
                <div class="pull-left image">
                    <img data-toggle="push-menu" src="{{ asset('adminlte/dist/img/avatar04.png') }}" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <a href="#"><i class="fa fa-circle text-primary"></i> Admin</a>
                </div>
            @endif

            @if (Auth::user()->hasRole('User'))
                <div class="pull-left image">
                    <img data-toggle="push-menu" src="{{ asset('adminlte/dist/img/avatar.png') }}" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <a href="#"><i class="fa fa-circle text-warning"></i> User</a>
                </div>

            @endif

        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">OPCIONES</li>

            <li id="tree-incidencia" class="treeview active">
                <a href="#">
                    <i class="fa fa-edit"></i> <span>Incidencia</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    @if (Auth::user()->hasRole('User'))
                        <li id="li-report"><a href="{{ route('report') }}"><i class="fa fa-flag-o"></i> Reportar</a></li>
                    @endif
                        <li id="li-record"><a href="{{ route('record') }}"><i class="fa fa-history"></i> Historial</a></li>
                </ul>
            </li>

            @if (Auth::user()->hasRole('Admin'))
            <li id="tree-usuario" class="treeview">
                <a href="#">
                    <i class="fa fa-user-secret"></i> <span>Usuario</span>
                    <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                        <li id="li-manage"><a href="{{ route('users.index') }}"><i class="fa fa-id-card"></i> Administrar</a></li>
                </ul>
            </li>
            @endif

        </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">

        </section>

        <!-- Main content -->
        <section class="content">

            <main class="py-4"  id="dynamic_content">

                @yield('content')
            </main>

        </section>
        <!-- /.content -->

    </div>
    <!-- /.content-wrapper -->

    <div class="control-sidebar-bg"></div>

</div>
<!-- ./wrapper -->

<!-- Scripts -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js" type="text/javascript"></script>
{{-- <script src="{{ asset('adminlte/bower_components/jquery/dist/jquery.min.js') }}"></script> --}}
<script src="{{ asset('adminlte/bower_components/fastclick/lib/fastclick.js') }}"></script>
<script src="{{ asset('adminlte/dist/js/adminlte.min.js') }}"></script>

@yield('scripts')

</body>
</html>
