@extends('layouts.sidebar')

@section('styles')
    <link rel="stylesheet" href="http://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <link href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.css" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<!-- Modal -->
<div class="modal fade" id="modalCenter" tabindex="-1" role="dialog" aria-labelledby="modalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalCenterLongTitle">Mas información</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form action="{{ route('incident.updateIncident') }}" method="POST">
            {{ method_field('PUT') }}
            {{ csrf_field() }}
                <div class="modal-body" id="modalCenterBody">
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">ID:</label>
                        <div class="col-sm-10">
                            <p id="modal_id" name="modal_id" readonly class="form-control-plaintext">No se encotraron los datos</p>
                        </div>
                    </div>
                    <input  id="parameter_id" name="parameter_id" type="hidden" value="500">
                    <hr>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Usuario:</label>
                        <div class="col-sm-10">
                            <p id="modal_users" readonly class="form-control-plaintext">No se encotraron los datos</p>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Categoría:</label>
                        <div class="col-sm-10">
                            <p id="modal_category" readonly class="form-control-plaintext">No se encotraron los datos</p>
                        </div>
                    </div>
                    <hr>
                    <h6>Asunto:</h6>
                        {{-- <p id="modal_subject">No se encotraron los datos</p> --}}
                        <input id="modal_subject" name="modal_subject" type="text" class="form-control" onkeypress="return event.keyCode!=13">
                    <br>
                    <h6>Primer mensaje:</h6>
                        {{-- <p id="modal_first_message">No se encotraron los datos</p> --}}
                        <input id="modal_id_message" name="modal_id_message" type="hidden">
                        <input id="modal_first_message" name="modal_first_message" type="text" class="form-control" onkeypress="return event.keyCode!=13">
                    <div class="pt-2">
                        <button title="Guardar cambios" id="btn_update" type="submit" class="btn btn-primary btn-sm float-right" onclick="return confirm('Esta seguro de modificar esta incidencia?')">Edición rápida <i class="fa fa-pencil-square-o"></i></button>
                    </div>
                    <br>
                    <hr>

                    <div class="row">
                        <div class="col-sm-9">
                            <div class="row">
                            <div class="col-8 col-sm-6">
                                <h6>Creado:</h6><p id="modal_created">No se encotraron los datos</p>
                            </div>
                            <div class="col-4 col-sm-6">
                                <h6>Modificado:</h6><p id="modal_update">No se encotraron los datos</p>
                            </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <a title="Eliminar" id="btn_delete" class="btn btn-danger mr-auto" onclick="return confirm('Esta seguro de eliminar esta incidencia?')" href=""><i class="fa fa-trash"></i></a>
                    @if (Auth::user()->hasRole('admin'))
                        <a title="Configuración" id="btn_config" class="btn btn-dark" href=""><i class="fa fa-cogs"></i></a>
                    @endif
                </div>

            </form>

        </div>
    </div>
</div>
<!-- Modal -->

<div class="card">
    <div class="card-header">
        Historial de Incidencias
    </div>

    <div class="card-body">
        <table class="table-hover row-border responsive nowrap" width="100%" id="data-table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Usuario</th>
                    <th>Categoria</th>
                    <th>Urgencia</th>
                    <th>Estado</th>
                    <th>Asunto</th>
                    <th>Fecha</th>
                    <th class="all">Opciones</th>
                </tr>
            </thead>
        </table>
    </div>

</div>
@endsection

@section('scripts')
<script src="http://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.js"></script>

<script type="text/javascript">
$.noConflict();
jQuery( document ).ready(function( $ ) {
    $(".treeview").first().addClass( "active" );
    $( '#li-record' ).addClass( "active" );

    var table = $('#data-table').DataTable({
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.18/i18n/Spanish.json"
        },
        // processing: true,
        // serverSide: true,
        ajax: '{!! route('incident.recordData') !!}',
        columns: [
            { title: 'ID', data: 'id', name: 'id' },
            { title: 'Usuario', data: 'users.0.name', name: 'users.0.name' },
            { title: 'Categoria', data: 'category', name: 'category' },
            { title: 'Urgencia', data: 'urgency', name: 'urgency' },
            { title: 'Estado', data: 'state', name: 'state' },
            { title: 'Asunto', data: 'subject', name: 'subject' },
            { title: 'Fecha', data: 'created_at', name: 'created_at' },
            { title: 'Opciones', data: null }
        ],
        columnDefs: [
            {
            // El signo (-) indica que se cuenta desde la ultima columna
            "targets": -1,
            "searchable": false,
            "orderable": false,
            "data": null,
            "render": function ( data, type, row, meta ) {
                return '<button title="Mas información" id="Detalle" type="button" class="btn btn-primary btn-sm"><i class="fa fa-info-circle"></i></button> <a title="Sala de chat" href="messages/' + data.id + '" class="btn btn-success btn-sm"><i class="fa fa-comments" aria-hidden="true"></i></a>';
                }
            },
        ],
        order: [[ 0, "desc" ]],

        // Diseño de los estados y urgencias en las incidencias
        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            if ( aData["state"] == "Nueva" )
            {
                $("td:eq(4)", nRow).html("<span class='badge badge-info btn-block'>Nueva</span>");
            }
            if ( aData["state"] == "En Progreso" )
            {
                $("td:eq(4)", nRow).html("<span class='badge badge-warning btn-block'>En Progreso</span>");
            }
            if ( aData["state"] == "Mensaje Nuevo" )
            {
                $("td:eq(4)", nRow).html("<span class='badge badge-success btn-block'>Mensaje Nuevo</span>");
            }
            if ( aData["state"] == "Anulado" )
            {
                $("td:eq(4)", nRow).html("<span class='badge badge-secondary btn-block'>Anulado</span>");
            }
            if ( aData["state"] == "Cerrada" )
            {
                $("td:eq(4)", nRow).html("<span class='badge badge-dark btn-block'>Cerrada</span>");
            }
            if ( aData["urgency"] == "Alta" )
            {
                $("td:eq(3)", nRow).html("<i class='fa fa-circle-o text-red'></i> <span>Alta</span>");
            }
            if ( aData["urgency"] == "Media" )
            {
                $("td:eq(3)", nRow).html("<i class='fa fa-circle-o text-yellow'></i> <span>Media</span>");
            }
            if ( aData["urgency"] == "Baja" )
            {
                $("td:eq(3)", nRow).html("<i class='fa fa-circle-o text-aqua'></i> <span>Baja</span>");
            }
        }
    });

    $('#data-table tbody').on( 'click', 'button', function () {
        var data = table.row( $(this).parents('tr') ).data();
        //Button Detalle
        if (this.id == 'Detalle') {
            //var data = table.row( this ).data();
            $("#modal_id").text(data.id);
            $("#parameter_id").val(data.id);
            $("#modal_users").text(data.users[0].name); // Se necesita mostrar todos los usuarios y no solo el primero (recorrer arreglo)
            $("#modal_category").text(data.category);
            $("#modal_subject").val(data.subject);
            $.get('/messages/api/'+data.id, function(data){
                // console.log(data[0].message);
                $("#modal_id_message").val(data[0].id);
                $("#modal_first_message").val(data[0].message);
            });
            $("#modal_created").text(data.created_at);
            $("#modal_update").text(data.updated_at);

            $("#btn_delete").attr('href','record/' + data.id);
            $("#btn_config").attr('href','record/' + data.id + '/tools');
            if(data.state =='Cerrada' || data.state =='Anulado'){
                $("#btn_delete").hide();
                $("#btn_update").hide();
            }
            else{
                $("#btn_delete").show();
                $("#btn_update").show();
            }

            $('#modalCenter').modal('show');


        }

    });

    // Recarga la tabla cada 30seg sin alterar la paginacion
    setInterval(function() {
        table.ajax.reload(null, false);
        console.log("Recarga");
    }, 30000 );

});
</script>


@endsection
