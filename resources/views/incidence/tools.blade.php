@extends('layouts.sidebar')


@section('content')

    @php
        $category = array("Hardware", "Software", "Estructural", "Otros");
        $urgency = array("Baja", "Media", "Alta");
        $state = array("Nueva", "En Progreso", "Mensaje Nuevo", "Anulado");
    @endphp

<div class="card">
    <div class="card-header">
        <h4>Incidencia #{{ $incidents->id }}</h4>
    </div>

    <div class="card-body">
        <form action="{{ route('incident.closingIncident', [$incidents->id]) }}" method="POST">
            {{ method_field('PUT') }}
            {{ csrf_field() }}

            <div class="form-group row">
                <label for="category" class="col-sm-2 col-form-label">Categoría: </label>
                <div class="col-sm-10">
                    <select class="custom-select" id="category_opt" name="category_opt">
                        @foreach ($category as $item)
                            @if ($item == $incidents->category)
                                <option value="{{ $item }}" selected>{{ $item }}</option>
                            @else
                                <option value="{{ $item }}">{{ $item }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="urgency" class="col-sm-2 col-form-label">Urgencia: </label>
                <div class="col-sm-10">
                    <select class="custom-select" id="urgency_opt" name="urgency_opt">
                        @foreach ($urgency as $item)
                            @if ($item == $incidents->urgency)
                                <option value="{{ $item }}" selected>{{ $item }}</option>
                            @else
                                <option value="{{ $item }}">{{ $item }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="state" class="col-sm-2 col-form-label">Estado: </label>
                <div class="col-sm-10">
                    <select class="custom-select" id="state_opt" name="state_opt">
                        @foreach ($state as $item)
                            @if ($incidents->state == 'Cerrada')
                                <option value="Cerrada" selected>Cerrada</option>
                            @endif
                            @if ($item == $incidents->state )
                                <option value="{{ $item }}" selected>{{ $item }}</option>
                            @else
                                <option value="{{ $item }}">{{ $item }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="subject" class="col-sm-2 col-form-label">Asunto: </label>
                <div class="col-sm-10">
                    <input type="subject" name="subject" id="subject" class="form-control" value="{{ old('subject', $incidents->subject) }}">
                </div>
            </div>

            <div class="form-group row">
                <label for="created_at" class="col-sm-2 col-form-label">Creado: </label>
                <div class="col-sm-10">
                    <input type="created_at" name="created_at" id="created_at" class="form-control" value="{{ old('created_at', $incidents->created_at) }}" disabled>
                </div>
            </div>

            <div class="form-group row">
                <label for="updated_at" class="col-sm-2 col-form-label">Actualizado: </label>
                <div class="col-sm-10">
                    <input type="updated_at" name="updated_at" id="updated_at" class="form-control" value="{{ old('updated_at', $incidents->updated_at) }}" disabled>
                </div>
            </div>

            @if (App\Incident::find($incidents->id)->hasClosing($incidents->id))
                <div class="form-group row">
                    <label for="datail_closing" class="col-sm-2 col-form-label">Detalle de cierre: </label>
                    <div class="col-sm-10">
                        <textarea type="datail_closing" name="datail_closing" id="datail_closing" class="form-control" rows="2" placeholder="Campo opcional solo para el cierre de incidencias">{{ old('datail_closing', $closing->datail_closing) }}</textarea>
                    </div>
                </div>

                {{-- Mostramos el nombre del usuario que cerro la incidencia --}}
                <div class="form-group row">
                    <label for="closed_by" class="col-sm-2 col-form-label">Cerrado por: </label>
                    <div class="col-sm-10">
                        <input type="text" name="closed_by" id="closed_by" class="form-control" value="{{ $closing->name }}">
                    </div>
                </div>

                <button type="submit" class="btn btn-danger float-right" name="revert_closing"  value="revert_closing" onclick="return confirm('Está seguro de re abrir la incidencia?')">Revertir cierre de incidencia</button>
            @else
                <div class="form-group row">
                    <label for="datail_closing" class="col-sm-2 col-form-label">Detalle de cierre: </label>
                    <div class="col-sm-10">
                        <textarea type="datail_closing" name="datail_closing" id="datail_closing" class="form-control" rows="2" placeholder="Campo opcional solo para el cierre de incidencias"></textarea>
                    </div>
                </div>

                <button type="submit" class="btn btn-primary" name="update" value="update">Actualizar incidencia</button>

                <button type="submit" class="btn btn-danger float-right" name="closing"  value="closing"onclick="return confirm('Antes de cerrar la incidencia verifique que los datos sean correctos. Desea proseguir?')">Cerrar incidencia</button>
            @endif
        </form>
    </div>

    <div class="card-footer">

            <a href="{{ route('record') }}">Regresar a la lista de incidencias</a>

    </div>
</div>
@endsection

@section('scripts')
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script type="text/javascript">

        jQuery( document ).ready(function( $ ) {

            if ( $("button[name='revert_closing']").length ) {
                $("input").prop('readonly', true);
                $("select").prop('disabled', true);
                $("textarea").prop('disabled', true);
            }

        });

        $(document).ready(function(){
            $('button[name ="closing"]').click(function(){
                $('button[name ="closing"]').hide();
            });
        });

    </script>
@endsection
