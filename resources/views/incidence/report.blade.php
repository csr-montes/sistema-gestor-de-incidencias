@extends('layouts.sidebar')


@section('content')

<div class="card">
    <div class="card-header">
        Reportar Incidencia
    </div>

    <form id="formReport" action="{{ route('reportIncident') }}" method="POST">
    {{ csrf_field() }}
    <div class="card-body">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text" for="category_opt">Categoría</label>
                    </div>
                    <select class="custom-select" id="category_opt" name="category_opt">
                        <option value="Hardware" selected>Hardware</option>
                        <option value="Software">Software</option>
                        <option value="Estructural">Estructural</option>
                        <option value="Otros">Otros</option>
                    </select>
                </div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label class="input-group-text" for="urgency_opt">Urgencia</label>
                    </div>
                    <select class="custom-select" id="urgency_opt" name="urgency_opt">
                        <option value="Baja" selected>Baja</option>
                        <option value="Media">Media</option>
                        <option value="Alta">Alta</option>
                    </select>
                </div>

                <div class="form-group">
                    <label class="col-form-label" for="subject_txt">Asunto:</label>
                    <input type="text" class="form-control" id="subject_txt" name="subject_txt">
                </div>

                <div class="form-group">
                    <label class="col-form-label" for="detail_txt">Detalles del problema:</label>
                    <textarea class="form-control" id="detail_txt" name="detail_txt"></textarea>
                </div>


    </div>
    <div class="card-footer text-right">
        <a href="{{route('record')}}" class="btn btn-secondary">Cerrar</a>
        <button id="btnSubmit" type="submit" class="btn btn-primary">Enviar</button>
    </div>
    </form>

</div>

@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<script>
    $(document).ready(function(){
        $(".treeview").first().addClass( "active" );
        $( '#li-report' ).addClass( "active" );

        $("#formReport").submit(function (e) {
            // e.preventDefault();  //stop submitting the form to see the disabled button effect
            $("#btnSubmit").attr("disabled", true); //disable the submit button
            return true;
        });
    });
</script>
@endsection
