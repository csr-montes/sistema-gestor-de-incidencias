@extends('layouts.sidebar')
@section('title', "User {$user->id}")
@section('content')

    <div class="card">
            <div class="card-header">
                <h4>Usuario #{{ $user->id }}</h4>
            </div>

            <div class="card-body">
                <p>Nombre de usuario: {{ $user->name }}</p>
                <p>Email: {{ $user->email }}</p>
            </div>

            <div class="card-footer">
                <a href="{{ route('users.index') }}">Regresar a la lista de usuarios</a>
            </div>
    </div>

@endsection

@section('scripts')

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<script type="text/javascript">
    $.noConflict();
    jQuery( document ).ready(function( $ ) {
        $( '#tree-incidencia' ).removeClass( "active" );
        $( '#tree-usuario' ).addClass( "active" );
        // $(".treeview").first().addClass( "active" );
        $( '#li-manage' ).addClass( "active" );
    });
</script>

@endsection