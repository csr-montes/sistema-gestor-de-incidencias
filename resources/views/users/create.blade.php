@extends('layouts.sidebar')

@section('title', "Create user")
@section('content')
    <div class="card">
        <h4 class="card-header">Crear usuario</h4>
        <div class="card-body">

            <form action="{{ url('users') }}" method="POST">
                {{ csrf_field() }}

                <div class="form-group">
                    <label for="name">Nombre: </label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="Alberto Ramos" value="{{ old('name') }}">
                </div>

                <div class="form-group">
                    <label for="email">Email: </label>
                    <input type="email" class="form-control" name="email" id="email" placeholder="example@example.com" value="{{ old('email') }}">
                </div>

                <div class="form-group">
                    <label for="password">Contraseña: </label>
                    <input type="password" class="form-control" name="password" id="password" placeholder="Contraseña">
                </div>

                <button type="submit" class="btn btn-primary" name="button1">Crear usuario</button>
            </form>

        </div>

        <div class="card-footer">
            <a href="{{ route('users.index') }}">Regresar a la lista de usuarios</a>
        </div>

    </div>

@endsection

@section('scripts')

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<script type="text/javascript">
    $.noConflict();
    jQuery( document ).ready(function( $ ) {
        $( '#tree-incidencia' ).removeClass( "active" );
        $( '#tree-usuario' ).addClass( "active" );
        // $(".treeview").first().addClass( "active" );
        $( '#li-manage' ).addClass( "active" );
    });
</script>

@endsection
