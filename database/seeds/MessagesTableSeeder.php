<?php

use Illuminate\Database\Seeder;

//
use App\Message;
use App\User;
use App\Incident;

class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_id_1 = User::where('email','admin')->first();
        $user_id_2 = User::where('email','user')->first();

        $incident_1 = Incident::where('urgency','Media')->first();

        $message = new Message();
        $message->message = 'Este es el primer mensaje que hace el .......... ............. ........ ................ .......... USER';
        $message->user_id = $user_id_2->id;
        $message->incident_id = $incident_1->id;
        $message->save();

        $message = new Message();
        $message->message = 'Este es el primer mensaje que hace el .......... ............. ........ ................ .......... ADMIN';
        $message->user_id = $user_id_1->id;
        $message->incident_id = $incident_1->id;
        $message->save();
    }
}
