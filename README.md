# Sistema Gestor de Incidencias


El sistema gestor de incidencias (SGI) es un aplicación web diseñada como propuesta para dar solución y optimizar la problemática de la gestión de incidencias dentro de organizaciones. 

### Documentación

El proyecto se desarrolló para el curso de "Proyecto de Software" de mi universad, bajo el marco de desarrollo agil de software SCRUM.

Puede revisar toda la documentación [aquí](https://docs.google.com/document/d/1_0DxaoheBYcL_cOZIF76JD7sYCs91z5hoXlizQiYDYU/edit?usp=sharing)


### Tecnología

SGI usa las siguientes tecnologías para funcionar correctamente:

* [Apache]
* [PHP]
* [MySQL]
* [Laravel]


### Instalación

SGI requiere levantar un servidor [Apache] para correr [PHP] v7.3+, ademas de tener la base de datos [MySQL] y el framework [Laravel]v5.8+.

Luego de clonar el proyecto debe establecer la consola dentro de la carpeta.
```sh
$ cd sgi
```
Dentro del carpeta del proyecto debemos ejecutar
```sh
$ composer install
```
También se debe editar el archivo .env según la configuración de su base de datos

Después debemos generar una APP_KEY
```sh
$ php artisan key:generate
```
Luego debemos recrear la base de datos
```sh
$ php artisan migrate:fresh --seed
```
Ahora ejecutamos el sevidor
```sh
$ php artisan serve
```

Finalmente puede revisar el proyecto en el navegador de su preferencia.

```sh
127.0.0.1:8000
```


### Video
[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/UsNSRk2WdDU/0.jpg)](https://www.youtube.com/watch?v=UsNSRk2WdDU)